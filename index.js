const H = require('highland');
const R = require('ramda');
const request = require('request');

const apiUrl = `https://itemstore-${process.env['NODE_ENV']}.inyourarea.co.uk`;
const jobsFeedUrl = `https://www.fish4.co.uk/feeds/JSONJobExtract.json`;
const type = 'jobs';

const parseResponse = require('highland-parse-response');
const deepEquals = require('deep-equals');

module.exports.handler = (event, context, callback) => {

    const formater = (items) => {
        let wrap = JSON.stringify({ items: JSON.parse(items) })
        let jsonItems = JSON.parse(wrap);
        return H((push, next) => {
            R.map(m => {
                return push(null, {
                    title: m.title,
                    description: m.description,
                    date: m.date,
                    endDate: m.endDate,
                    origin_id: m.referencenumber,
                    location: m.location,
                    lat: parseFloat(m.latitude) || 0,
                    lng: parseFloat(m.longitude) || 0,
                    company: m.company,
                    url: m.url,
                    logo: m.logo,
                    salary: m.salary,
                    sector: m.sector,
                    hours: m.hours,
                    contractType: m["Contract Type"]
                })
            })(jsonItems.items);
            console.log(jsonItems.items.length)
            return push(null, H.nil);
        });
    };

    return H.wrapCallback(request)({
        url: jobsFeedUrl
    })
        .flatMap(parseResponse)
        .flatMap(items => {
            return formater(items)
                .flatMap(item => {
                    return H.wrapCallback(request)({
                        url: [apiUrl, 'items', type].join('/'),
                        qs: { origin_id: item.origin_id, lat: item.lat, lng: item.lng, radius: 1, units: 'm' },
                        json: true
                    })
                        .flatMap(parseResponse)
                        .flatMap(prevItems => {
                            console.log("searching: " + item.origin_id)
                            if (prevItems.length && deepEquals(item, R.head(prevItems).item)) {
                                return H(['Nothing to update - report has not changed']);
                            }
                            console.log("adding: " + item.origin_id)
                            return H.wrapCallback(request)(R.merge({
                                url: [apiUrl, 'items', type].join('/'),
                            }, prevItems.length ? {
                                json: R.merge(item, {
                                    publishedTime: R.head(prevItems).publishedTime
                                }),
                                method: 'put',
                                qs: { origin_id: item.origin_id, lat: item.lat, lng: item.lng, radius: 1, units: 'm', updateAll: 1 }
                            } : {
                                        json: item,
                                        method: 'post'
                                    }))
                                .flatMap(parseResponse);
                        });
                })
                .collect();
        })
        .toCallback(callback);
};

if (!module.parent) {
    module.exports.handler(null, null, (error, result) => { console.log(error, result); });
}
