#!/bin/bash

echo "Normalising repository"
git checkout master
git pull --rebase origin master
git pull --rebase deploy master

echo "Pushing to deployment server for test deployment"
git push deploy master
echo "Pulling updated terraform state"
git pull --rebase origin master

if [[ "${1}" == "stag" ]]; then
    echo "Creating a staging tag"
    git tag stag-`date +%s`
fi

if [[ "${1}" == "prod" ]]; then
    echo "Creating a production tag"
    git tag prod-`date +%s`
fi

echo "Pushing to deployment server with tags"
git push deploy master --tags
echo "Pulling updated terraform state"
git pull --rebase origin master
