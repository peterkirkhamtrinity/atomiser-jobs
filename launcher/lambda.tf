resource "aws_lambda_function" "lambda" {
    s3_bucket          = "${var.lambda_s3_bucket}"
    s3_key             = "${var.lambda_s3_key}"
    function_name      = "${var.application_name}_${terraform.env}"
    role               = "${aws_iam_role.lambda_role.arn}"
    handler            = "index.handler"
    runtime            = "nodejs6.10"
    memory_size        = 512
    timeout            = 300
    environment {
        variables = {
            NODE_ENV = "${terraform.env}"
        }
    }
    tags {
        Name = "${var.application_name}"
        Environment = "${terraform.env}"
    }
}

resource "aws_iam_role" "lambda_role" {
    name = "${var.application_name}_${terraform.env}_lambda_role"
    assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
POLICY
}

resource "aws_iam_role_policy" "lambda_policy" {
    name = "${var.application_name}_${terraform.env}_iam_policy"
    role = "${aws_iam_role.lambda_role.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "logs:*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}
EOF
}
resource "aws_cloudwatch_event_rule" "every_one_hour" {
    name = "${var.application_name}-${terraform.env}-every_one_hour"
    description = "Fires every one hour"
    schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "lambda_every_one_hour" {
    rule = "${aws_cloudwatch_event_rule.every_one_hour.name}"
    target_id = "lambda"
    arn = "${aws_lambda_function.lambda.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.lambda.function_name}"
    principal = "events.amazonaws.com"
    source_arn = "${aws_cloudwatch_event_rule.every_one_hour.arn}"
}
