#!/usr/bin/env bash

ENVIRONMENT=${1}

if [ -z "${ENVIRONMENT}" ]; then
    ENVIRONMENT="test"
fi

cd "$(dirname "$0")"

NAME="atomiser-publicnotices"
PREFIX="${NAME}"
COMMIT=$( git log | grep "^commit [0-9a-f]\{40\}$" | sed -e 's/commit //' | head -n 1 )
FILE="${COMMIT}.zip"

export TF_VAR_application_name="${NAME}"
export TF_VAR_application_slug="${PREFIX}"
export TF_VAR_application_description="An ingester for public notices"
export TF_VAR_lambda_s3_bucket="tm-ep-lambda-functions"
export TF_VAR_lambda_s3_key="${TF_VAR_application_slug}/${TF_VAR_environment}/${FILE}"
export TF_VAR_region="eu-west-1"
export TF_VAR_commit="${COMMIT}"

# Zip lamda function
cd ..
rm *.zip
zip -r ${FILE} *
aws s3 cp --region eu-west-1 ${FILE} s3://${TF_VAR_lambda_s3_bucket}/${TF_VAR_lambda_s3_key}

# Run terraform plan
cd launcher
terraform env select ${ENVIRONMENT} && terraform plan && terraform apply
git add .terraform terraform.tfstate.d && git commit -m "Update TFSATE" && git pull --rebase origin master && git push origin master
