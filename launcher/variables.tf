variable "region" {
    default = "eu-west-1"
}

variable "account_id" {
    default = "247237293916"
}

variable "lambda_s3_bucket" {
    default = "tm-ep-lambda-functions"
}

variable "lambda_s3_key" {
    default = "blank-function.zip"
}

variable "application_slug" {
    default = "atomiser-fixmystreet"
}

variable "application_name" {
    default = "atomiser-fixmystreet"
}

variable "application_description" {
    default = "The fixmystreet data ingestion and atomising function"
}